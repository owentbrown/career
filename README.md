# career

Using machine learning to increase human wages and capital


## The objective 
The objective of the project is to help humans select the right skills to develop, to get them in the best paying jobs possible.

We do this using Machine Learning.

## Innovation
- **Need**: People need to know how much other people with similar skill levels are earning.
- **Approach**: Let users upload a resume or link to a LinkedIn profile, and get a prediction of how much they will earn in their next job. Then, tell them how much they would earn in their next job if they could add additional skills. For example, developers fluent in a language might find that simply learning a hot, in-demand framework could boost their earnings 10k. 
- **Competition**: Currently, users must interview colleaguse, speak with recruiters, and eventually apply for jobs, simply to know their market worth. Their process involves triangulating salary data from several sources, while intepretting cues from hiring managers and recruiters. 
- **Benefits**: Visiting a single webpage to get this information, gives a person a valuable data point, and helps them know how to allocate their limited education and recruiting budget.
- **Cost**: Visiting our single web page saves considerable time over visiting many web pages and talking to many recruiters. It's easier to use than salary.com or glassdoor. Unlike Linkedin.com, it's free.

## Web interface
The resulting project will have a web interface. Users will upload a resume, or link to a LinkedIn profile. They receive their results.

